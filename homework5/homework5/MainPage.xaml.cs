﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Diagnostics;
using Xamarin.Forms.Maps;
using Xamarin.Essentials;
namespace homework5
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
          InitializeComponent();
            InitMap();
            PlaceAMarker();
        }
        public void InitMap()
        {
            Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.1295, -117.1596), Distance.FromMiles(1)));
        }
        private void Button_Clicked(object sender, EventArgs e)
        {
            Map.MapType = MapType.Satellite;
            B1.BackgroundColor = Color.Blue;
            B2.BackgroundColor = Color.Black;
            B3.BackgroundColor = Color.Black;
        }

        private void Button_Clicked_1(object sender, EventArgs e)
        {
            Map.MapType = MapType.Hybrid;
            B1.BackgroundColor = Color.Black;
            B2.BackgroundColor = Color.Blue;
            B3.BackgroundColor = Color.Black;
        }

        private void Button_Clicked_2(object sender, EventArgs e)
        {
            Map.MapType = MapType.Street;
            B1.BackgroundColor = Color.Black;
            B2.BackgroundColor = Color.Black;
            B3.BackgroundColor = Color.Blue;
        }

        private void PlaceAMarker()
        {
            var PopeyesPos = new Position(33.4758, -117.1243);
            var KFCPos = new Position(33.134250, -117.121220);
            var McDonaldsPos = new Position(33.013480, -117.078594);
            var ChilisPos = new Position(33.502952, -117.149817);
            var ChurchsPos = new Position(33.183970, -117.368750);
            var Popeyes = new Pin
            {
                Type = PinType.Place,
                Position = PopeyesPos,
                Label = "Popeyes",
                Address = "30679 Temecula Pkwy, Temecula, CA 92592"
            };

            var KFC = new Pin
            {
                Type = PinType.Place,
                Position = KFCPos,
                Label = "KFC",
                Address = "771 Center Dr, San Marcos, CA 92069"
            };
            var Mcdonalds = new Pin
            {
                Type = PinType.Place,
                Position = McDonaldsPos,
                Label = "McDonald's",
                Address = "16440 Bernardo Center Dr, San Diego, CA 92128"
            };
            var Chilis = new Pin
            {
                Type = PinType.Place,
                Position = ChilisPos,
                Label = "Chili's",
                Address = "27645 Ynez Rd, Temecula, CA 92591"
            };
            var Churchs = new Pin
            {
                Type = PinType.Place,
                Position = ChurchsPos,
                Label = "Church's Chicken",
                Address = "510 Oceanside Blvd, Oceanside, CA 92054"
            };

            Map.Pins.Add(Popeyes);
            Map.Pins.Add(KFC);
            Map.Pins.Add(Mcdonalds);
            Map.Pins.Add(Chilis);
            Map.Pins.Add(Churchs);
        }


        private void Picker_SelectedIndexChanged(object sender, EventArgs e)
        {
            picker.Title = "Picker";
            picker.TitleColor = Color.Red;
            var item = (Picker)sender;
            int selected = item.SelectedIndex;
            if(selected == 0)
            {
                Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.4758, -117.1243), Distance.FromMiles(1)));
            }
            else if(selected == 1)
            {
                Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.134250, -117.121220), Distance.FromMiles(1)));
            }
            else if(selected == 2)
            {
                Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.013480, -117.078594), Distance.FromMiles(1)));
            }
            else if(selected == 3)
            {
                Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.502952, -117.149817), Distance.FromMiles(1)));
            }
            else if(selected == 4)
            {
                Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.183970, -117.368750), Distance.FromMiles(1)));
            }
            picker.Title = "Picker";
            picker.TitleColor = Color.Red;
        }

        private void ContentPage_Appearing(object sender, EventArgs e)
        {
            B3.BackgroundColor = Color.Blue;
        }
    }
}
